package notebook;

import org.junit.Assert;
import org.junit.Test;

import com.ftl.notebook.core.model.note.Note;
import com.ftl.notebook.core.model.university.Course;
import com.ftl.notebook.core.model.university.Department;
import com.ftl.notebook.core.model.university.University;

public class RepoNodeTest {

	@Test
	public void testFullPath(){
		
		Course course = new  Course();
 		University university = new University();
		Department department = new Department();
		Note note = new Note();
		university.setId(3l);
		university.setCode("ToA");
		department.setUniversity(university);
		department.setId(2l);
		department.setCode("CS");
		course.setDepartment(department);
		course.setCode("Domes");
		course.setId(1l);
		note.setId(8l);
		note.setCourse(course);
//		note.setCode("Simiosis-1");
		
		Assert.assertEquals(university.getNodeFullPath(), "universities/ToA");
		Assert.assertEquals(department.getNodeFullPath(), "universities/ToA/departments/CS");
		Assert.assertEquals(course.getNodeFullPath(),	"universities/ToA/departments/CS/courses/Domes");
		Assert.assertEquals(note.getNodeFullPath(), 	"universities/ToA/departments/CS/courses/Domes/notes/8");
	}
}
