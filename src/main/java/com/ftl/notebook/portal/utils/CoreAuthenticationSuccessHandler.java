package com.ftl.notebook.portal.utils;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

public class CoreAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler  {

	@Override
	public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse res, Authentication auth)
			throws IOException, ServletException {
		//TODO: FIX THIS SHIT
		if (req.getParameter("prevUri") != null) {
			System.out.println("Modal Login");
			String prevUri = req.getParameter("prevUri");
			
			String redirectUri;
			if ("/login".equals(prevUri))
				redirectUri = "home";
			else {
				redirectUri = prevUri;
			}
			getRedirectStrategy().sendRedirect(req, res, redirectUri);
		} else {
			System.out.println("Normal Login");
			super.onAuthenticationSuccess(req, res, auth);
		}
	}

}
