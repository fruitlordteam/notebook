package com.ftl.notebook.portal.session;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.ftl.core.model.CoreUser;
import com.ftl.core.utils.CoreConstant;

@Component
@Scope(value = CoreConstant.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SessionBean {

	private CoreUser coreUser = new CoreUser();

	public CoreUser getUser() {
		return coreUser;
	}

	public void setUser(CoreUser coreUser) {
		this.coreUser = coreUser;
	}
	
}
