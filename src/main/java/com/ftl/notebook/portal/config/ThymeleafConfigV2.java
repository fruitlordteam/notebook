package com.ftl.notebook.portal.config;

import static com.ftl.core.utils.CoreConstant.HTML5;
import static com.ftl.core.utils.CoreConstant.UTF8;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

import nz.net.ultraq.thymeleaf.LayoutDialect;

@Configuration
public class ThymeleafConfigV2 {

	private static final String PROPERTY_THYMELEAF_CACHEABLE = "th.cache";
	
	private static final String FOLDER_EMAILS = "/WEB-INF/mail/";
	private static final String SUFFIX_HTML = ".html";
	private static final String FOLDER_TEMPLATES = "/WEB-INF/templates/";

	@Autowired	
	private Environment env;
	
	@Bean
	public SpringTemplateEngine templateEngine() {
		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.addTemplateResolver(webTemplateResolver());
		templateEngine.addTemplateResolver(emailTemplateResolver());
		templateEngine.addDialect(layoutDialect());
		templateEngine.addDialect(securityDialect());
		templateEngine.addDialect(java8TimeDialect());
		return templateEngine;
	}
	
	// ThymeLeaf + Spring
	@Bean
	public SpringResourceTemplateResolver webTemplateResolver() {
		SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
		templateResolver.setPrefix(FOLDER_TEMPLATES);
		templateResolver.setSuffix(SUFFIX_HTML);
		templateResolver.setTemplateMode(HTML5);
//		templateResolver.setCharacterEncoding(UTF8);
		templateResolver.setCacheable(Boolean.getBoolean(env.getProperty(PROPERTY_THYMELEAF_CACHEABLE))); // DEVELOPMENT - Hot Deploy
		templateResolver.setOrder(2);
		
		return templateResolver;
	}
	
	/**
	 * THYMELEAF: Template Resolver for email templates.
	 */
	@Bean
	public ClassLoaderTemplateResolver emailTemplateResolver() {
		ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setPrefix(FOLDER_EMAILS);
	    templateResolver.setTemplateMode(HTML5);
	    templateResolver.setOrder(1);
	    return templateResolver;
	}


	@Bean
	public ThymeleafViewResolver viewResolver() {
		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
		viewResolver.setTemplateEngine(templateEngine());
		viewResolver.setOrder(1);
		viewResolver.setCharacterEncoding(UTF8);
		return viewResolver;
	}
	
	/**
	 * Custom Layout Dialect tags(layout:)
	 * <a href = "https://github.com/ultraq/thymeleaf-layout-dialect"> Layout Doc </a>
	 * You can also try using Apache Tiles for layouts.
	 * @return
	 */
	@Bean
	public LayoutDialect layoutDialect() {
		return new LayoutDialect();
	}
	
	/**
	 * Dialect for Sring Security tags (sec:)
	 * <a href = "https://github.com/thymeleaf/thymeleaf-extras-springsecurity"> Thymeleaf Doc </a>
	 */
	@Bean
	public SpringSecurityDialect securityDialect()  {
		return new SpringSecurityDialect();
	}
	
	/**
	 * Dialect for Thymeleaf Java8 Date Formatting. 
	 * https://github.com/thymeleaf/thymeleaf-extras-java8time
	 */
	@Bean
	public Java8TimeDialect java8TimeDialect() {
		return new Java8TimeDialect();
	}
}
