//package com.ftl.notebook.portal.config;
//
//
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.ApplicationContextAware;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.ViewResolver;
//import org.springframework.web.servlet.config.annotation.EnableWebMvc;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
//import org.thymeleaf.TemplateEngine;
//import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
//import org.thymeleaf.spring4.SpringTemplateEngine;
//import org.thymeleaf.spring4.templateresolver.SpringResourceTemplateResolver;
//import org.thymeleaf.spring4.view.ThymeleafViewResolver;
//import org.thymeleaf.templatemode.TemplateMode;
//import org.thymeleaf.templateresolver.ITemplateResolver;
//
//import nz.net.ultraq.thymeleaf.LayoutDialect;
//
///**
// * Thymeleaf 3 Java configuration
// * http://www.thymeleaf.org/doc/articles/thymeleaf3migration.html
// * https://github.com/jmiguelsamper/thymeleaf3-spring-helloword
// * 
// * TODO: Change to Apache Tiles 3 or wait Layout Dialect.
// * @author Petros Siatos
// *
// */
//@Configuration
//@EnableWebMvc
//public class ThymeleafConfigV3 extends WebMvcConfigurerAdapter implements ApplicationContextAware {
// 
//    private ApplicationContext applicationContext;
// 
//    public void setApplicationContext(ApplicationContext applicationContext) {
//        this.applicationContext = applicationContext;
//    }
//
//	@Bean
//	public ViewResolver viewResolver() {
//		ThymeleafViewResolver resolver = new ThymeleafViewResolver();
//		resolver.setTemplateEngine(templateEngine());
//		resolver.setCharacterEncoding("UTF-8");
//		resolver.setOrder(1);
//		return resolver;
//	}
//	
//	@Bean
//	public TemplateEngine templateEngine() {
//		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
//		templateEngine.setTemplateResolver(templateResolver());
//		templateEngine.addDialect(layoutDialect());
//		templateEngine.addDialect(securityDialect());
//		return templateEngine;
//	}
//	
//	@Bean
//	public ITemplateResolver templateResolver() {
//        SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
//        resolver.setApplicationContext(applicationContext);
//        resolver.setPrefix("/WEB-INF/templates/");
//        resolver.setSuffix(".html");
//        resolver.setTemplateMode(TemplateMode.HTML);
//        return resolver;
//    }
//	
//	/**
//	 * Custom Layout Dialect tags(layout:)
//	 * <a href = "https://github.com/ultraq/thymeleaf-layout-dialect"> Layout Doc </a>
//	 * You can also try using Apache Tiles for layouts.
//	 * @return
//	 */
//	@Bean
//	public LayoutDialect layoutDialect() {
//		return new LayoutDialect();
//	}
//	
//	/**
//	 * Dialect for Sring Security tags (sec:)
//	 * <a href = "https://github.com/thymeleaf/thymeleaf-extras-springsecurity"> Thymeleaf Doc </a>
//	 */
//	@Bean
//	public SpringSecurityDialect securityDialect()  {
//		return new SpringSecurityDialect();
//	}
//}
