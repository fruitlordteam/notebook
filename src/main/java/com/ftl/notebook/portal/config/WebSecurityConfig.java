package com.ftl.notebook.portal.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.ftl.core.service.CoreUserService;
import com.ftl.notebook.portal.utils.CoreAuthenticationSuccessHandler;

/**
 * Java Configuration for Spring Security
 * using a custom UserDetailsService implementation.
 * <a href = "http://docs.spring.io/spring-security/site/docs/4.0.4.CI-SNAPSHOT/reference/htmlsingle/#core-services-jdbc-user-service" > Spring Doc </a>
 * 
 * @author Petros Siatos
 *
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled  = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private static final int PASSWORD_STRENGTH = 16;

	@Autowired
	private CoreUserService coreUserService;

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(PASSWORD_STRENGTH);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
//				.antMatchers("/img/**", "/js/**", "/css/**", "/webjars/**", "/home", "/login", "/register").permitAll()
				.antMatchers("/admin/**").hasRole("ADMIN")
				.anyRequest().permitAll()
		.and()
			.formLogin()
			.loginPage("/login")
			.successHandler(new CoreAuthenticationSuccessHandler());
	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth
			 .userDetailsService(coreUserService)
			 .passwordEncoder(passwordEncoder());
	}

}
