package com.ftl.notebook.portal.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

import com.ftl.notebook.core.config.NotebookModelConfig;

/**
 * Java Config Spring Context for Notebook Portal.
 * 
 * @author Petros Siatos
 *
 */
@Configuration
@ComponentScan({NotebookPortalConfig.PACKAGE_ROOT})
@Import({ NotebookModelConfig.class })
@PropertySource("classpath:properties/app-${spring.profiles.active:development}.properties")
public class NotebookPortalConfig {
	
	public static final String PACKAGE_ROOT = "com.ftl.notebook.portal";
	
	@Autowired
	public Environment env;
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
}
