package com.ftl.notebook.portal.config;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import com.ftl.notebook.portal.controller.NoteController;
import com.ftl.oak.model.UploadFile;

public class GeneralMappingExceptionResolver extends SimpleMappingExceptionResolver {

	 @Override
	 public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception exception) {

	    if(exception instanceof MultipartException) {
	        String error = "Max filesize exceeded, please ensure filesize is below 5mb.";
	        HashMap<String, Object> model = new HashMap<String, Object>(2);
	        model.put("error", error);
	        model.put("errors", error);
	        
	        ModelAndView mnv = new ModelAndView(NoteController.VIEW_UPLOAD_FORM);
	        
//	        Collections.list(request.getAttributeNames())
//	        		.forEach(attr -> {
//	        			System.out.println(attr);
//	        			System.out.println(request.getAttribute(attr));
//	        		});
	        
//	        String requestUri = request.getAttribute("javax.servlet.error.request_uri").toString();
	        
	        mnv.addObject(new UploadFile());
	        mnv.addObject("error", "qq");
	        mnv.addObject("errors", "4qs");
	        
			return mnv;
	    } else {
	        return super.resolveException(request, response, handler, exception); // Do whatever default behaviour is (ie throw to error page).
	    }
	}
}