package com.ftl.notebook.portal.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.ftl.core.model.CorePage;
import com.ftl.core.model.CoreUser;
//import com.ftl.core.model.dto.UserLoginInfo;
import com.ftl.core.utils.CoreSecurityUtils;

@ControllerAdvice(PortalControllerAdvisor.PATH_CONTROLLER)		 
public class PortalControllerAdvisor {

	public static final String PATH_CONTROLLER = "com.ftl.notebook.portal.controller";
	
	// Models
	public static final String MODEL_USER			= "user";
	public static final String MODEL_MENU			= "menu";
	public static final String MODEL_LOGIN 			= "loginInfo";
	
	private static final List<CorePage> MENU_LIST;
	
 	static {
 		MENU_LIST = new ArrayList<CorePage>();
		
 		MENU_LIST.add(new CorePage("home",		"/home"));
 		MENU_LIST.add(new CorePage("login", 	"/login"));
 		MENU_LIST.add(new CorePage("register", 	"/register"));
 	}
 	

//    @ExceptionHandler(SizeLimitExceededException.class)
//    @ResponseBody
//    public ModelAndView handleControllerException(HttpServletRequest request, Throwable ex) {
////        HttpStatus status = getStatus(request);
//        
//    	System.out.println("We piasame u");
//    	
//        ModelAndView mnv = new ModelAndView();
//		mnv.addObject(new UploadFile());
//		mnv.addObject("error", "omg");
//		mnv.addObject("errors", "omg QQQQQ");
//		mnv.setViewName(NoteController.VIEW_UPLOAD_FORM);
//        
//        return mnv;
//    }

    private HttpStatus getStatus(HttpServletRequest request) {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        if (statusCode == null) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.valueOf(statusCode);
    }
 	
	@ModelAttribute(MODEL_MENU)
	public List<CorePage> getMenu() {
		return MENU_LIST;
	}
	
	@ModelAttribute(MODEL_USER)
	public CoreUser getUser() {
		return CoreSecurityUtils.getLoggedUser().orElse(null);
	}
	
}