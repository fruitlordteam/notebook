
package com.ftl.notebook.portal.controller;

import java.io.IOException;
import java.util.Optional;

import javax.jcr.RepositoryException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ftl.core.annotation.Get;
import com.ftl.core.annotation.Post;
import com.ftl.core.model.CoreParam;
import com.ftl.core.model.dto.CorePageWrapper;
import com.ftl.core.utils.CoreSecurityUtils;
import com.ftl.notebook.core.model.embeddable.Rating;
import com.ftl.notebook.core.model.note.Note;
import com.ftl.notebook.core.model.note.StarRatingType;
import com.ftl.notebook.core.model.university.Course;
import com.ftl.notebook.core.model.university.Department;
import com.ftl.notebook.core.model.university.University;
import com.ftl.notebook.core.service.CourseService;
import com.ftl.notebook.core.service.DepartmentService;
import com.ftl.notebook.core.service.NoteService;
import com.ftl.notebook.core.service.UniversityService;
import com.ftl.notebook.portal.controller.ajax.NoteAjaxController;

import javassist.NotFoundException;

@RequestMapping(CourseController.ROUTE_ROOT)
@Controller
public class CourseController extends CrudController {
	
	// Routes
	public static final String ROUTE_ROOT 			= "/universities/{uniCode}/departments/{deptCode}/courses";
	
	// Views
	public static final String VIEW_DISPLAY 		= "/courses/display";
	public static final String VIEW_DISPLAY_LIST	= "/courses/displayList";
	public static final String VIEW_ADD_FORM 		= "/courses/addForm";
	public static final String VIEW_REDIRECT_ROOT	= "redirect:/universities/{uniCode}/departments/{deptCode}";
	
	// HTML
	public static final String HTML_COMMENT_LIST	= "#noteListContainer";
	
	// Models
	public static final String MODEL_COURSES		= "courses";
	public static final String MODEL_NOTES			= "notes";
	public static final String MODEL_PAGE_WRAPPER	= "pagewrapper";

	// Request Parameters
	public static final String PARAM_COURSE_ID	= "courseId";

	
	@Autowired private UniversityService universityService;
	@Autowired private DepartmentService departmentService;
	@Autowired private CourseService courseService;
	@Autowired private NoteService noteService;
	
	@Get
	public ModelAndView displayAll(@PathVariable String deptCode, ModelAndView mnv) {
		mnv.addObject(MODEL_COURSES, courseService.findByDepartmentCode(deptCode));
		mnv.setViewName(VIEW_DISPLAY_LIST);
		return mnv;
	}
	
	@Get(ROUTE_DISPLAY)
	public ModelAndView display(@PathVariable String uniCode, @PathVariable String deptCode, @PathVariable String code, ModelAndView mnv) throws NotFoundException {
		
		Optional<University> university = universityService.getByCode(uniCode);
		Optional<Department> department = departmentService.getWithCourses(university.orElseThrow(() -> new NotFoundException("University not found.")), deptCode);
		
		Optional<Course> course = courseService.getByDepartment(department.orElseThrow(() -> new NotFoundException("Department not found.")), code);
		mnv.addObject(course.orElseThrow(() -> new NotFoundException("Course not found.")));
		
		Page<Note> page = noteService.findByCourse_Id(new PageRequest(0, 10, new Sort(Direction.DESC, Rating.RATING_APPROX)), course.get().getId());
		
		CoreParam<Long> param = CoreParam.createCoreParam(PARAM_COURSE_ID, course.get().getId());
		CorePageWrapper<Note> wrapper = new CorePageWrapper<Note>(page, NoteAjaxController.ROUTE_EXTERNAL_GET, HTML_COMMENT_LIST, param);
		wrapper.setAfterSuccess(StarRatingType.JAVASCRIPT_FUNCTION_INITIALIZE);
		
		mnv.addObject(MODEL_PAGE_WRAPPER, wrapper);
		
		mnv.setViewName(VIEW_DISPLAY);
		return mnv;
	}
	
	@Get(ROUTE_ADD)
	@PreAuthorize(CoreSecurityUtils.HAS_ROLE_ADMIN)
	public ModelAndView add(@PathVariable String uniCode, @PathVariable String deptCode, ModelAndView mnv) throws NotFoundException {
	
		Optional<University> university = universityService.getByCode(uniCode);
		Optional<Department> department = departmentService.get(university.orElseThrow(() -> new NotFoundException("University not found.")), deptCode);
		
		Course course = new Course();
		course.setDepartment(department.orElseThrow(() -> new NotFoundException("Department not found.")));
		mnv.addObject(course);
		mnv.setViewName(VIEW_ADD_FORM);
		
		return mnv;
	}

	@Post(ROUTE_ADD)
	@PreAuthorize(CoreSecurityUtils.HAS_ROLE_ADMIN)
	public ModelAndView submit(ModelAndView mnv, @Valid @ModelAttribute Course course, BindingResult result) throws RepositoryException, IOException {

		if (result.hasErrors()) {
			result.getAllErrors().forEach(error -> System.out.println("Form Error: " + error.toString()));
			mnv.setViewName(VIEW_ADD_FORM);

		} else {
			courseService.add(course);
			mnv.setViewName(CourseController.VIEW_REDIRECT_ROOT);

		}
		return mnv;
	}
	 
}
