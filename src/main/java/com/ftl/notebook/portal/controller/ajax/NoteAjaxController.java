package com.ftl.notebook.portal.controller.ajax;

import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ftl.core.annotation.CoreController;
import com.ftl.core.annotation.Get;
import com.ftl.core.annotation.Post;
import com.ftl.core.model.CoreParam;
import com.ftl.core.model.CoreUser;
import com.ftl.core.model.dto.CorePageWrapper;
import com.ftl.core.utils.CoreConstant;
import com.ftl.core.utils.CoreSecurityUtils;
import com.ftl.notebook.core.model.note.Note;
import com.ftl.notebook.core.model.note.NoteRating;
import com.ftl.notebook.core.model.note.StarRatingType;
import com.ftl.notebook.core.service.NoteRatingService;
import com.ftl.notebook.core.service.NoteService;

@CoreController(NoteAjaxController.ROUTE_ROOT)
public class NoteAjaxController {

	// Routes
	public static final String ROUTE_ROOT = "/notes/ajax";
	public static final String ROUTE_GET = "get";
	public static final String ROUTE_RATE = "rate/{id}";
	
	// External Routes
	public static final String ROUTE_EXTERNAL_GET = String.join(CoreConstant.STR_SLASH, ROUTE_ROOT, ROUTE_GET);
	public static final String ROUTE_EXTERNAL_RATE = String.join(CoreConstant.STR_SLASH, ROUTE_ROOT, ROUTE_RATE);
	
	// Views - Fragments
	public static final String VIEW_FRAGMENT_DISPLAY_LIST 	= "/notes/displayList :: noteList";
	
	// HTML
	public static final String HTML_COMMENT_LIST	= "#noteListContainer";
	
	// Models
	public static final String MODEL_PAGE_WRAPPER= "pagewrapper";
	
	// Request Parameters
	public static final String PARAM_COURSE_ID	= "courseId";
	
	@Autowired private NoteService 		noteService;
	@Autowired private NoteRatingService noteRatingService;

	//
	// Ratings
	//
	@Post(ROUTE_RATE)
	public void rate(@PathVariable Long id, @RequestParam int value, HttpServletResponse response) {
		
		// Get as Request Param? or @Valid @ModelAttribute NoteRating
		Note note = noteService.find(id);
		Optional<CoreUser> user = CoreSecurityUtils.getLoggedUser();
		
		if (note != null && user.isPresent()) {
			
			noteRatingService.addRating(new NoteRating(note, user.get(), StarRatingType.valueOf(value)));
			
			response.setStatus(HttpServletResponse.SC_OK);
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}
	
	@Get(ROUTE_GET)
	public ModelAndView  getPage(@RequestParam Long courseId, Pageable pageable, ModelAndView mnv) {
	   Page<Note> page = noteService.findByCourse_Id(pageable, courseId);
	   
	   CoreParam<Long> param = CoreParam.createCoreParam(PARAM_COURSE_ID, courseId);
	   CorePageWrapper<Note> wrapper = new CorePageWrapper<Note>(page, ROUTE_EXTERNAL_GET, HTML_COMMENT_LIST, param);
	   wrapper.setAfterSuccess(StarRatingType.JAVASCRIPT_FUNCTION_INITIALIZE);
	   
	   mnv.addObject(MODEL_PAGE_WRAPPER, wrapper);
	   mnv.setViewName(VIEW_FRAGMENT_DISPLAY_LIST);
	   
	   return mnv;
	}

}
