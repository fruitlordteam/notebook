
package com.ftl.notebook.portal.controller;

import java.io.IOException;
import java.util.Optional;
import java.util.StringJoiner;

import javax.jcr.RepositoryException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import com.ftl.core.annotation.CoreController;
import com.ftl.core.annotation.Get;
import com.ftl.core.annotation.Post;
import com.ftl.core.utils.CoreConstant;
import com.ftl.core.utils.CoreSecurityUtils;
import com.ftl.notebook.core.model.university.University;
import com.ftl.notebook.core.service.UniversityService;

@CoreController(UniversityController.ROUTE_ROOT)
public class UniversityController extends CrudController {

	// Routes
	public static final String ROUTE_ROOT 			= "/universities";
	public static final String ROUTE_GET			= "/display";
	public static final String ROUTE_AJAX_ADD		= "/ajax/add";
	
	// Views
	public static final String VIEW_DISPLAY				= "/universities/display";
	public static final String VIEW_DISPLAY_LIST		= "/universities/displayList";
	public static final String VIEW_ADD_FORM			= "/universities/addForm";
	public static final String VIEW_REDIRECT_ROOT		= "redirect:/universities";
	
	// Views - Fragments
	public static final String VIEW_FRAGMENT_ADD_FORM	= "/universities/addForm :: universityForm";
	
	
	// Models
	public static final String MODEL_UNIVERSITY			= "university";
	public static final String MODEL_DEPARTMENT			= "department";
	public static final String MODEL_UNIVERSITIES		= "universities";
	public static final String MODEL_DEPARTMENTS		= "departments";
	
	@Autowired private UniversityService universityService;
//	@Autowired private DepartmentService departmentService;
	
	@ModelAttribute
	public University getUniversity() {
		University university = new University();
		return university;
	}
	
	@Get
	public ModelAndView displayAll(ModelAndView mnv) {
		mnv.addObject(MODEL_UNIVERSITIES, universityService.findAll());
		mnv.setViewName(VIEW_DISPLAY_LIST);
		return mnv;
	}
	
	@Get(ROUTE_DISPLAY)
	public ModelAndView display(@PathVariable String code,	ModelAndView mnv) {
		
		Optional<University> university = universityService.getByCodeWithDepartments(code);
		
		university.ifPresent((u) -> {
			mnv.addObject(u);
			mnv.addObject(MODEL_DEPARTMENTS, u.getDepartments());
		});

		mnv.setViewName(VIEW_DISPLAY);
		return mnv;
	}
	
	/**
	 * Redirects home page form to the display page of the university
	 * @param university
	 * @return
	 */
	@Post(ROUTE_GET)
	public String redirectToDisplay(@ModelAttribute University university) {
		return new StringJoiner(CoreConstant.STR_SLASH).add(VIEW_REDIRECT_ROOT).add(university.getCode()).toString();
	}

	@Get(ROUTE_ADD)
	@PreAuthorize(CoreSecurityUtils.HAS_ROLE_ADMIN)
	public ModelAndView add(ModelAndView mnv) {
		
		mnv.setViewName(VIEW_ADD_FORM);
		return mnv;
	}
	
	@Get(ROUTE_REMOVE)
	public ModelAndView remove(@PathVariable Long id, ModelAndView mnv) {
		universityService.delete(id);
		mnv.setViewName(VIEW_REDIRECT_ROOT);
		
		return mnv;
	}
	
	@Post(ROUTE_ADD)
	@PreAuthorize(CoreSecurityUtils.HAS_ROLE_ADMIN)
	public ModelAndView submit(ModelAndView mnv, @Valid @ModelAttribute University university, BindingResult result) throws RepositoryException, IOException {

		if (result.hasErrors()) {
			result.getAllErrors().forEach(error -> System.out.println("Form Error: " + error.toString()));
			mnv.setViewName(VIEW_ADD_FORM);

		} else {
			universityService.add(university);
			mnv.setViewName(VIEW_REDIRECT_ROOT);
		}
		return mnv;
	}
	
	@Post(ROUTE_AJAX_ADD)
	@Secured(CoreSecurityUtils.ROLE_ADMIN)
	public String ajaxSubmit(@Valid @ModelAttribute University university, BindingResult result) throws RepositoryException, IOException  {
		
		if (result.hasErrors()) {
			result.getAllErrors().forEach(error -> System.out.println("Form Error: " + error.toString()));
		} else {
			universityService.add(university);
		}
		
		return VIEW_FRAGMENT_ADD_FORM;
	}
	 
}
