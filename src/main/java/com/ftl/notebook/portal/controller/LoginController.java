package com.ftl.notebook.portal.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;

import com.ftl.core.annotation.CoreController;
import com.ftl.core.annotation.Get;

@CoreController(LoginController.ROUTE_ROOT)
public class LoginController {

	// Routes
	public static final String ROUTE_ROOT = "/login";

	// Views
	public static final String VIEW_DISPLAY = "/login";
	public static final String VIEW_REDIRECT_HOME = "redirect:home";

//	@Autowired
//	private CoreUserService coreUserService;
	
	@Get
	public ModelAndView loginForm(HttpServletRequest request, ModelAndView mnv) {
	    mnv.setViewName(VIEW_DISPLAY);
		return mnv;
	} 
//	@Post
//	public String loginSubmit(@Valid @ModelAttribute(PortalControllerAdvisor.MODEL_LOGIN) CoreUserLoginInfo loginInfo, BindingResult result) {
//
//		if (result.hasErrors()) {
//			result.getAllErrors().forEach(error -> System.out.println("Form Error: " + error.getCode()));
//			return VIEW_DISPLAY;
//		} else {
//			Optional<CoreUser> validatedUser = coreUserService.getValidated(loginInfo);
//
//			if (validatedUser.isPresent()) {
//				sessionBean.setUser(validatedUser.get());
//
//				return VIEW_REDIRECT_HOME;
//			} else {
//				result.addError(new ObjectError(CoreUser.NAME, "Wrong name or password!"));
//				result.addError(new ObjectError(CoreUser.PASSWORD, "Wrong name or password!"));
//				return VIEW_DISPLAY;
//			}
//		}
//	}
	
}
