package com.ftl.notebook.portal.controller;

abstract public class CrudController {

		// Routes
		public static final String ROUTE_ADD 			= "/add";
		public static final String ROUTE_REMOVE			= "/remove/{id}";
		public static final String ROUTE_DISPLAY 		= "/{code}";
//		public static final String ROUTE_DISPLAY_LIST 	= "/displayList";
		
//		@Get(ROUTE_DISPLAY)
//		abstract public ModelAndView display(@PathVariable Long id,	ModelAndView mnv);
//		
//		@Get(ROUTE_ADD)
//		abstract public ModelAndView add(ModelAndView mnv);
//		
//		@Post(ROUTE_ADD)
//		abstract public ModelAndView submit(ModelAndView mnv, @Valid @ModelAttribute Course course, BindingResult result);
}
