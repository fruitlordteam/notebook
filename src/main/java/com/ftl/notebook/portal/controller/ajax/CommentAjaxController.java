package com.ftl.notebook.portal.controller.ajax;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ftl.core.annotation.CoreController;
import com.ftl.core.annotation.Delete;
import com.ftl.core.annotation.Get;
import com.ftl.core.annotation.Post;
import com.ftl.core.model.CoreParam;
import com.ftl.core.model.dto.CorePageWrapper;
import com.ftl.core.utils.CoreConstant;
import com.ftl.core.utils.CoreSecurityUtils;
import com.ftl.notebook.core.model.note.Comment;
import com.ftl.notebook.core.service.CommentService;

@CoreController(CommentAjaxController.ROUTE_ROOT)
public class CommentAjaxController {

	// Routes
//	public static final String ROUTE_ROOT = "/universities/{uniCode}/departments/{deptCode}/courses/{courseCode}/notes/{noteId}/comments";
	public static final String ROUTE_ROOT = "/comments";
	
	public static final String ROUTE_ADD = "ajax/add";
	public static final String ROUTE_GET = "ajax/get";
	public static final String ROUTE_DELETE = "/ajax/delete/{id}";
	
	// External
	public static final String ROUTE_EXTERNAL_AJAX_GET = String.join(CoreConstant.STR_SLASH, ROUTE_ROOT, ROUTE_GET);
	
	// Views - Fragments
	public static final String VIEW_FRAGMENT_ADD_FORM 		= "/comments/addForm :: commentForm";
	public static final String VIEW_FRAGMENT_DISPLAY_ITEM 	= "/comments/displayList :: commentItem";
	public static final String VIEW_FRAGMENT_DISPLAY_LIST 	= "/comments/displayList :: commentList";
	
	// HTML
	public static final String HTML_COMMENT_LIST= "#commentListContainer";
	
	// Models
	public static final String MODEL_PAGE_WRAPPER= "pagewrapper";
	
	// Request Parameters
	public static final String PARAM_NOTE_ID	= "noteId";
	
	@Autowired private CommentService 	commentService;
	
	@Post(ROUTE_ADD)
//	@PreAuthorize(CoreSecurityUtils.HAS_ROLE_USER)
	public ModelAndView addComment(@Valid @ModelAttribute Comment comment, BindingResult result, ModelAndView mnv, HttpServletResponse response) {

		if (!result.hasErrors()) {
			mnv.addObject(commentService.save(comment));
			mnv.setViewName(VIEW_FRAGMENT_DISPLAY_ITEM);
			response.setStatus(HttpServletResponse.SC_OK);
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			mnv.setViewName(VIEW_FRAGMENT_ADD_FORM);
		}
		
		return mnv;
	}
	
	@Delete(ROUTE_DELETE)
	public void deleteComment(@PathVariable Long id, HttpServletResponse response) {

		Comment comment = commentService.find(id);
		
		if (comment != null && CoreSecurityUtils.isLoggedIn(comment.getCreatedBy())) {
			commentService.delete(comment);
			response.setStatus(HttpServletResponse.SC_OK);
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			System.out.println("Who let the dogs out?");
		}
	}
	
//	@Get
//	public @ResponseBody DatatablesResponse<Comment> findAll(@DatatablesParams DatatablesCriterias cr) {
//	   DataSet<Comment> dataSet = commentService.findWithDatatablesCriterias(cr);
//	   return DatatablesResponse.build(dataSet, cr);
//	}
	
	@Get(ROUTE_GET)
	public ModelAndView  getPage(@RequestParam Long noteId, Pageable pageable, ModelAndView mnv) {
	   Page<Comment> page = commentService.findByNote_Id(pageable, noteId);
	   
	   CoreParam<Long> param = CoreParam.createCoreParam(PARAM_NOTE_ID, noteId);
	   CorePageWrapper<Comment> wrapper = new CorePageWrapper<Comment>(page, ROUTE_EXTERNAL_AJAX_GET, HTML_COMMENT_LIST, param);
	   
	   mnv.addObject(MODEL_PAGE_WRAPPER, wrapper);
	   mnv.setViewName(VIEW_FRAGMENT_DISPLAY_LIST);
	   
	   return mnv;
	}

}
