package com.ftl.notebook.portal.controller;

import org.springframework.web.servlet.ModelAndView;

import com.ftl.core.annotation.CoreController;
import com.ftl.core.annotation.Get;

@CoreController(AdminController.ROUTE_ROOT)
public class AdminController {

	// Routes
	public static final String ROUTE_ROOT 			= "/admin";

	// Views
	public static final String VIEW_DISPLAY			= "/admin/display";
	
	@Get
	public ModelAndView display(ModelAndView mnv) {
		mnv.setViewName(VIEW_DISPLAY);
		return mnv;
	}
	
}