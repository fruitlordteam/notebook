package com.ftl.notebook.portal.controller;

import java.io.IOException;
import java.io.InputStream;

import javax.jcr.ItemExistsException;
import javax.jcr.RepositoryException;
import javax.validation.Valid;

import org.apache.tomcat.util.http.fileupload.FileUploadBase.SizeLimitExceededException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ftl.core.annotation.CoreController;
import com.ftl.core.annotation.Get;
import com.ftl.core.annotation.Post;
import com.ftl.core.model.CoreParam;
import com.ftl.core.model.dto.CorePageWrapper;
import com.ftl.core.utils.CoreSecurityUtils;
import com.ftl.notebook.core.model.note.Comment;
import com.ftl.notebook.core.model.note.Note;
import com.ftl.notebook.core.service.CommentService;
import com.ftl.notebook.core.service.CourseService;
import com.ftl.notebook.core.service.NoteRatingService;
import com.ftl.notebook.core.service.NoteService;
import com.ftl.notebook.portal.controller.ajax.CommentAjaxController;
import com.ftl.oak.model.RepoFile;
import com.ftl.oak.model.UploadFile;

import javassist.NotFoundException;

@CoreController(NoteController.ROUTE_ROOT)
public class NoteController extends CrudController {

	// Routes
	public static final String ROUTE_ROOT = "/universities/{uniCode}/departments/{deptCode}/courses/{courseCode}/notes";
	public static final String ROUTE_DISPLAY 		= "/{id}";
	public static final String ROUTE_DOWNLOAD = "/download/{id}";
	public static final String ROUTE_REDIRECT_ROOT = "redirect:/universities/{uniCode}/departments/{deptCode}/courses/{courseCode}";
	public static final String ROUTE_AJAX_RATE = "/ajax/rate/{id}";
	
	public static final String ROUTE_AJAX_GET = "ajax/get";
	
	// Views
	public static final String VIEW_DISPLAY 		= "/notes/display";
	public static final String VIEW_DISPLAY_LIST 	= "/notes/displayList";
	public static final String VIEW_UPLOAD_FORM		= "/notes/uploadForm";

	// Views - Fragments
	public static final String VIEW_FRAGMENT_DISPLAY_LIST 	= "/notes/displayList :: noteList";
	
	// HTML
	public static final String HTML_COMMENT_LIST	= "#commentListContainer";
	
	// Models
	public static final String MODEL_NOTES		= "notes";
	public static final String MODEL_COMMENTS	= "comments";
	public static final String MODEL_PAGE_WRAPPER= "pagewrapper";
	public static final String MODEL_USER_RATING = "userRating";
	
	// Request Parameters
	public static final String PARAM_NOTE_ID	= "noteId";
	
	// Response Parameters 
	private static final String HEADER_CONTENT_DISPOSITION = "Content-Disposition";
	private static final String FORMAT_ATTACHMENT_FILENAME = "attachment; filename=\"%s\"";
	
	
	@Autowired private NoteService 		noteService;
	@Autowired private NoteRatingService noteRatingService;
	@Autowired private CommentService 	commentService;
	@Autowired private CourseService 	courseService;
	
	@Get
	public ModelAndView noteForm(@PathVariable String courseCode, ModelAndView mnv) {
		mnv.addObject(MODEL_NOTES, noteService.findByCourseCode(courseCode));
		mnv.setViewName(VIEW_DISPLAY_LIST);
		return mnv;
	}
	
	@Get(ROUTE_DISPLAY)
	public ModelAndView display(@PathVariable Long id, ModelAndView mnv) throws NotFoundException {
		
		Note note = noteService.findById(id).orElseThrow(() -> new NotFoundException("Note note found."));
		
		CoreSecurityUtils.getLoggedUser()
				.flatMap(user -> noteRatingService.findByNoteAndUser(note, user))
				.ifPresent(nr -> mnv.addObject(MODEL_USER_RATING, nr.getRating()));
		
		Comment comment = new Comment();
		comment.setNote(note);
		
		mnv.addObject(note);
		mnv.addObject(comment);
		
		Page<Comment> page = commentService.findByNote_Id(new PageRequest(0, 10, new Sort(Direction.DESC, Comment.CREATED_DATE)), note.getId());

		CoreParam<Long> param = CoreParam.createCoreParam(PARAM_NOTE_ID, note.getId());
		CorePageWrapper<Comment> wrapper = new CorePageWrapper<Comment>(page, CommentAjaxController.ROUTE_EXTERNAL_AJAX_GET, HTML_COMMENT_LIST, param);
		
		mnv.addObject(MODEL_PAGE_WRAPPER, wrapper);
		mnv.setViewName(VIEW_DISPLAY);
		return mnv;
		
	}
	
	@Get(ROUTE_ADD)
	@PreAuthorize(CoreSecurityUtils.HAS_ROLE_USER)
	public ModelAndView add(ModelAndView mnv) {
		mnv.addObject(new UploadFile());
		mnv.setViewName(VIEW_UPLOAD_FORM);
		return mnv;
	}
	
	@Post(ROUTE_ADD)
	@PreAuthorize(CoreSecurityUtils.HAS_ROLE_USER)
	public String handleFormUpload(@PathVariable String courseCode, @Valid @ModelAttribute UploadFile uploadFile, BindingResult result) throws RepositoryException, IOException {

		if (result.hasErrors()) {
			return VIEW_UPLOAD_FORM;
		} else {
			
			//TODO: Service job.
			Note note = new Note();
			note.setFile(new RepoFile(uploadFile.getFile()));
			note.setCourse(courseService.findByCode(courseCode));
			note.setDescription(uploadFile.getDescription());
			noteService.add(note);
//			note.setCode(uploadFile.getFile().getOriginalFilename());

			return ROUTE_REDIRECT_ROOT;
		}
	}
    
    /**
     * http://websystique.com/springmvc/spring-mvc-4-file-download-example/
     * @param noteCode
     * @param response
     * @throws IOException 
     * @throws RepositoryException 
     * @throws Exception
     */
	@Get(ROUTE_DOWNLOAD)
	@ResponseBody
    public ResponseEntity<InputStreamResource> download(@PathVariable Long id) throws IOException, RepositoryException {
    	
		Note note = noteService.find(id);
		
		if (note != null) {
			noteService.retrieveFromRepo(note);
		}
        
        /* "Content-Disposition : inline" will show viewable types [like images/text/pdf/anything viewable by browser] right on browser 
            while others(zip e.g) will be directly downloaded [may provide save as popup, based on your browser setting.]*/
//        response.setHeader("Content-Disposition", String.format("inline; filename=\"" + note.getFile().getDescription() +"\""));
         
        /* "Content-Disposition : attachment" will be directly download, may provide save as popup, based on your browser setting*/
//        response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", note.getFile().getDescription()));
         
		String contentType = note.getFile().getMultipartFile().getContentType();
		Integer contentLength = (int) note.getFile().getMultipartFile().getSize();
		InputStream inputStream = note.getFile().getMultipartFile().getInputStream();
		
//        InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
 
        //Copy bytes from source to destination(outputstream in this example), closes both streams.
//        FileCopyUtils.copy(note.getFile().getMultipartFile().getInputStream(), response.getOutputStream());
        
        return ResponseEntity.ok()
        		.contentLength(contentLength)
        		.contentType(MediaType.valueOf(contentType))
        		.header(HEADER_CONTENT_DISPOSITION, String.format(FORMAT_ATTACHMENT_FILENAME, note.getFile().getDescription()))
        		.body(new InputStreamResource(inputStream));
        					
    }

	//
	// Exception Handlers
	//
	@ExceptionHandler
	public String handleException(Exception ex) {
		System.out.println("Excellent exception handling");
		System.out.println(ex.toString());
		
		return ROUTE_REDIRECT_ROOT;
	}
	
	@ExceptionHandler
	public String handleException(ItemExistsException ex, ModelAndView mnv) {
		System.out.println("Item Already Exists");
		System.out.println(ex.toString());
		
		return ROUTE_REDIRECT_ROOT;
	}
	
	@ExceptionHandler()
	public String handleException(SizeLimitExceededException ex) {
		System.out.println("Too long dude");
		return ROUTE_REDIRECT_ROOT;
	}
}
