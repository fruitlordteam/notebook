package com.ftl.notebook.portal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ftl.core.annotation.CoreController;
import com.ftl.notebook.core.model.university.University;
import com.ftl.notebook.core.service.UniversityService;

@CoreController(HomeController.ROUTE_ROOT)
public class HomeController {
	
	// Routes
	public static final String ROUTE_ROOT 		= "/home";

	// Views
	public static final String VIEW_DISPLAY			= "/home";
	
	// Models
	public static final String MODEL_UNIVERSITIES		= "universities";
	
	@Autowired private UniversityService universityService;
	
    @RequestMapping
    public String home(Model model) {
    	model.addAttribute(new University());
    	model.addAttribute(MODEL_UNIVERSITIES, universityService.findAll());
    	return VIEW_DISPLAY;
    }
    
}
