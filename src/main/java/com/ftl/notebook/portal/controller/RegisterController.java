package com.ftl.notebook.portal.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ftl.core.annotation.CoreController;
import com.ftl.core.model.dto.UserRegisterInfo;
import com.ftl.core.service.CoreUserService;

@CoreController(RegisterController.ROUTE_ROOT)
public class RegisterController {

	// Routes
	public static final String ROUTE_ROOT = "/register";

	// Views
	public static final String VIEW_DISPLAY = "/register";
	public static final String VIEW_REDIRECT_LOGIN = "redirect:login";

	// Models
	public static final String MODEL_REGISTER_INFO = "registerInfo";
	
	@Autowired private CoreUserService coreUserService;

	@ModelAttribute(MODEL_REGISTER_INFO)
	public UserRegisterInfo getRegisterInfo() {
		return new UserRegisterInfo();
	}

	@RequestMapping(method = RequestMethod.GET)
	public String registerForm() {
		return VIEW_DISPLAY;
	}

	@RequestMapping(method = RequestMethod.POST)
	public String registerSubmit(@Valid @ModelAttribute(MODEL_REGISTER_INFO) UserRegisterInfo registerInfo, BindingResult result) {

		if (result.hasErrors()) {
			return VIEW_DISPLAY;
		} else {
			coreUserService.register(registerInfo);
			return VIEW_REDIRECT_LOGIN;
		}
	}
}
