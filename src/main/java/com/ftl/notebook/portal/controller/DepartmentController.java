
package com.ftl.notebook.portal.controller;

import java.io.IOException;
import java.util.Optional;

import javax.jcr.RepositoryException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ftl.core.annotation.Get;
import com.ftl.core.annotation.Post;
import com.ftl.core.utils.CoreSecurityUtils;
import com.ftl.notebook.core.model.university.Department;
import com.ftl.notebook.core.model.university.University;
import com.ftl.notebook.core.service.DepartmentService;
import com.ftl.notebook.core.service.UniversityService;

import javassist.NotFoundException;

@RequestMapping(DepartmentController.ROUTE_ROOT)
@Controller
public class DepartmentController extends CrudController {

	// Routes
	public static final String ROUTE_ROOT 		= "/universities/{uniCode}/departments";
//	public static final String[] ROUTE_ROOT 		= {"/universities/{uniCode}/departments", "/departments"};

	// Views
	public static final String VIEW_ADD_FORM		= "/departments/addForm";
	public static final String VIEW_DISPLAY			= "/departments/display";
	public static final String VIEW_DISPLAY_LIST	= "/departments/displayList";
	public static final String VIEW_REDIRECT_ROOT	= "redirect:/universities/{uniCode}";
	
	// Models
	public static final String MODEL_DEPARTMENTS		= "departments";
	public static final String MODEL_COURSES			= "courses";
	
	@Autowired private UniversityService universityService;
	@Autowired private DepartmentService departmentService;

	@Get
	public ModelAndView displayAll(@PathVariable String uniCode, ModelAndView mnv) {
		mnv.addObject(MODEL_DEPARTMENTS, departmentService.findByUniversityCode(uniCode));
		mnv.setViewName(VIEW_DISPLAY_LIST);
		return mnv;
	}
	
	@Get(ROUTE_DISPLAY)
	public ModelAndView display(@PathVariable String uniCode, @PathVariable String code, ModelAndView mnv) throws NotFoundException {
		
		Optional<University> university = universityService.getByCode(uniCode);
		Optional<Department> department = departmentService.getWithCourses(university.orElseThrow(() -> new NotFoundException("University not found.")), code);
		
		mnv.addObject(department.orElseThrow(() -> new NotFoundException("Department not found.")));
		mnv.addObject(MODEL_COURSES, department.get().getCourses());

		mnv.setViewName(VIEW_DISPLAY);
		return mnv;
	}
	
	@Get(ROUTE_ADD)
	@PreAuthorize(CoreSecurityUtils.HAS_ROLE_ADMIN)
	public ModelAndView add(@PathVariable String uniCode, ModelAndView mnv) throws NotFoundException {
		
		Optional<University> university = universityService.getByCode(uniCode);
		
		Department department = new Department();
		department.setUniversity(university.orElseThrow(() -> new NotFoundException("University not found.")));
		mnv.addObject(department);
		mnv.setViewName(VIEW_ADD_FORM);
		return mnv;
	}

	@Post(ROUTE_ADD)
	@PreAuthorize(CoreSecurityUtils.HAS_ROLE_ADMIN)
	public ModelAndView submit(ModelAndView mnv, @Valid @ModelAttribute Department department, BindingResult result) throws RepositoryException, IOException {

		if (result.hasErrors()) {
			result.getAllErrors().forEach(error -> System.out.println("Form Error: " + error.toString()));
			mnv.setViewName(VIEW_ADD_FORM);

		} else {	
			departmentService.add(department);
			mnv.setViewName(VIEW_REDIRECT_ROOT);

		}
		return mnv;
	}
	 
}
