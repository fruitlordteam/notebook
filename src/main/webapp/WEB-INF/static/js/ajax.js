// On Start
$(function () {
	
	// Comments Add
	enableFormAjaxPost("#commentAddForm", "/comments/ajax/add", commentAddSuccess, commentAddError);
	
	// Comments Delete
	$("#commentListContainer [id^='commentForm']").each(function() {
		var url = "/comments/ajax/delete/"
		url += $(this).attr("id").slice(11);
		enableFormAjaxSubmit('#' + $(this).attr('id'), url, "delete", commentDeleteSuccess);
	});
	
});

function commentAddSuccess(data) {
	$("#commentNew").html(data);
	$("#commentList").prepend("<li id='commentNew'></li>");
}

function commentAddError(data) {
	$("#commentAddFormContainer").replaceWith(data.responseText);
	commentAddAjax();
}

function commentDeleteSuccess(data, formId) {
	$(formId).parent().remove();
}

/***********************
		UTILITIES
***********************/
function enableFormAjaxPost(formId, ajaxUrl, onSuccess, onError) {
	enableFormAjaxSubmit(formId, ajaxUrl, "post", onSuccess, onError)
}

function enableFormAjaxSubmit(formId, ajaxUrl, ajaxType, onSuccess, onError) {
//	console.log("EnableFormAjaxSubmit: " + formId + " " + ajaxUrl + " " + ajaxType + " " + onSuccess + " " + onError);
	
	$(formId).submit(function(event) {

		// Prevent the form from submitting via the browser.
		event.preventDefault();
	
		var form = $(formId);
		var data = form.serialize();
	
		var headers = {};
		addCsrf(headers);
		
		$.ajax({
			type: ajaxType,
			headers: headers,
		    url: ajaxUrl,
		    data: data,
			dataType : 'html',
			timeout : 5000,
			success : function(data) {
				if (typeof onSuccess === 'function') {
					onSuccess(data, formId);
				} else {
					$(formId).html(data.responseText);
					
				}
			},
			error : function(data) {
				console.log("ERROR");
				if (typeof onError === 'function') { 
					onError(data, formId);
				} else {
					$(formId).html(data.responseText);
				}
			},
			done : function(e) {
				console.log("DONE");
			}
		});
	});
}


function linkAjaxPost(linkID, reloadID, ajaxUrl, onSuccess, onError) {
	linkAjaxClick(linkID, reloadID, ajaxUrl, "post", onSuccess, onError);
}

function linkAjaxGet(linkID, reloadID, ajaxUrl, onSuccess, onError, afterSuccess) {
	linkAjaxClick(linkID, reloadID, ajaxUrl, "get", onSuccess, onError, afterSuccess);
}

function linkAjaxClick(linkID, reloadID, ajaxUrl, ajaxType, onSuccess, onError, afterSuccess) {
	
//	console.log("Link ajax Submit: " + linkID + " " + reloadID + " " + ajaxUrl)
	console.log(afterSuccess);
	
	$(linkID).click( function(e) {
		e.preventDefault();
		
		simpleAjax(ajaxUrl, ajaxType, 'json', null, reloadID, onSuccess, onError, afterSuccess);
		
	});
	
}

function simpleAjax(ajaxUrl, ajaxType, ajaxDataType, ajaxData, reloadID, onSuccess, onError, afterSuccess) {
	
	var headers = {};
	addCsrf(headers);
	
	$.ajax({
		type: ajaxType,
		headers: headers,
	    url: ajaxUrl,
	    data: ajaxData,
	    dataType : 'html',
		timeout : 5000,
		success : function(data) {
			if (typeof onSuccess === 'function') {
				onSuccess(data, reloadID);
			} else {
				$(reloadID).html(data);
				
			}
			if (typeof afterSuccess === 'function') {
				afterSuccess();
			}
		},
		error : function(data) {
			if (typeof onError === 'function') { 
				onError(data, reloadID);
			} else {
				$(reloadID).html(data.responseText);
			}
		}
	});
}

function addCsrf(headers) {
	// Include the CSRF Token - Spring Security
	var csrfHeader = $("meta[name='_csrf_header']").attr("content");
	var csrfToken = $("meta[name='_csrf']").attr("content");
	headers[csrfHeader] = csrfToken;
}

// https://stackoverflow.com/questions/1184624/convert-form-data-to-javascript-object-with-jquery/1186309#1186309
$.fn.serializeObject = function(name) {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			
			o[this.name].push(this.value || '');
		} else {
			
			// Ignore attributes that start with _ like _csrf.
			if (!this.name.match("^_")) {
				o[this.name] = this.value || '';
			}
		}
	});
	
	var data = {};
	data[name] = o;
	
	return data;
};