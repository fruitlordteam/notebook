// On Start
$(function () {
	
	srInitialize();
	
});

function srInitialize() {
	$('.star-rating').srCreate();
}

$.fn.srCreate = function() {
	$(this).each(function() {
		$(this).html("<span class='star'></span><span class='star'></span><span class='star'></span><span class='star'></span><span class='star'></span>")
		
		var value = $(this).attr('value');
		
		if(typeof value !== typeof undefined && value != 0) {
			
			$(this).srDisable();
			var starChildren = $($(this).children('.star').get().reverse()).slice(0, value);
			starChildren.addClass('active');
			
			// If it has a 0.5
			if (value % 1.0 == 0.5) {
				starChildren.last().prev().addClass('active half');
			}
		}
	});
}

$.fn.srEnable = function() {
	$(this).removeClass('disabled').addClass('active');
}

$.fn.srDisable = function() {
	$(this).removeClass('active').addClass('disabled');
//	$(this).children('.star').off('click');
}

$.fn.srToggle = function() {
	$(this).toggleClass('disabled').toggleClass('active');
}

$.fn.srOnUpdate = function(callback) {
	$(this).children('.star').each(function() {
//		console.log($(this).attr('value'));
		var starValue = $(this).nextAll('.star').length + 1;
		$(this).click(function() {
			$(this).addClass('active');
			$(this).nextAll('.star').addClass('active');
			$(this).parent('.star-rating').srDisable();
			callback(starValue);
		});
	});
}